package com.lwise.NewsScrap.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsItemList {
    private String title;
    private String  contents;
}
