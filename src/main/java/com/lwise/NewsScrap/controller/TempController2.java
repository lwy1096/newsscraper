package com.lwise.NewsScrap.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/money")

public class TempController2 {
    @GetMapping("/get-pay-back")
    public String peoplePayBack(){
        return "환불되셨습니다, 고객님";
    }
    @GetMapping("/get-change-product")
    public String peopleChange(){
        return "교환되셨습니다.";
    }
}
