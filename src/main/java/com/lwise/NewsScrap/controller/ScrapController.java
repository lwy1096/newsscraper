package com.lwise.NewsScrap.controller;

import com.lwise.NewsScrap.model.NewsItemList;
import com.lwise.NewsScrap.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;
    @GetMapping("/html")
    public List<NewsItemList> getHtml() throws IOException {
        return scrapService.run();

    }

}
