package com.lwise.NewsScrap.controller;

import com.lwise.NewsScrap.model.NewsItemList;
import com.lwise.NewsScrap.service.NewsScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController2 {
    private final NewsScrapService newsScrapService;
    @GetMapping("/html2")
    public List<NewsItemList> getHtml() throws IOException {
        return newsScrapService.run();
    }
}
