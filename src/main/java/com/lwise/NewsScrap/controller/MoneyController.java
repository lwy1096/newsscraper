package com.lwise.NewsScrap.controller;

import com.lwise.NewsScrap.model.MoneyChangeRequest;
import com.lwise.NewsScrap.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money") //()는 이름표
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;
    //서비스가 시작할 때 같이 구동

    @PostMapping("/change") //()는 이름표
    public String peopleChange(@RequestBody MoneyChangeRequest request) {
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }
    @GetMapping("/pay-back") //()는 이름표
    public String peoplePayBack() {
        //public 자리는 접근 자료 권한.
        // String 자리는 자료의 유형,
        // peoplePayBack()는 자료의 이름, 자료의 이름(){} 즉 메서드(함수)다.
        return "환불되셨습니다, 고객님";
    }
}
