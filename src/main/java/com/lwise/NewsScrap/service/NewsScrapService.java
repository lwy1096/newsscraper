package com.lwise.NewsScrap.service;

import com.lwise.NewsScrap.model.MoneyChangeRequest;
import com.lwise.NewsScrap.model.NewsItemList;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class NewsScrapService {
    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/";
        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        return document;
    }
    private List<Element> parseHtml( Document document ) {
        Elements elements = document.getElementsByClass("auto-article");

        List<Element> tempResult = new LinkedList<>();

        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                tempResult.add(item2);
            }
        }
        return tempResult;
    }


    private List<NewsItemList> makeResult(List<Element> list) {
        List<NewsItemList> result = new LinkedList<>();
        for (Element item : list) {
            Elements checkTheContents = item.getElementsByClass("flow-hidden");
            if (checkTheContents.size() == 0){
                Elements checkTheHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkTheHiddenBanner.size() > 0) {
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    String content = item.getElementsByClass("auto-fontB").get(0).text();

                   NewsItemList addItem = new NewsItemList();
                    addItem.setTitle(title);
                    addItem.setContents(content);

                    result.add(addItem);
                }
            }
        }
        return result;
    }

    public List<NewsItemList> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<NewsItemList> result = makeResult(elements);

        return result;
    }
}
